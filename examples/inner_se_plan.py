# start of automatically prepended lines
from beamlinetools.BEAMLINE_CONFIG import *
# end of automatically prepended lines
from bluesky.plans import scan
from bluesky.plan_stubs import sleep
from bluesky.utils import (
    separate_devices,
    all_safe_rewind,
    Msg,
    ensure_generator,
    short_uid as _short_uid,
)
import bluesky.plan_stubs as bps
from bluesky import preprocessors as bpp
import time
from ophyd.status import Status


"""
    assumed gas flows
    h = 12 ml/min
    hel = 10 ml /min
    c0 =3ml/min

    Assumed that mfc1 is H2, mfc2 is He, MFC3 is CO
"""

# Define the gas mix for a given gas selection in ml/min
gas_flows = {"He":(12, 0,0),"H2":(0,12,0), "CO":(0,0,3)}

def set_gas(mfc_tuple):
    yield from mv(gas_dosing.massflow_contr1, mfc_tuple[0],gas_dosing.massflow_contr2, mfc_tuple[1],gas_dosing.massflow_contr3, mfc_tuple[2])
    
def ramp_and_xrd(gas_sel, temp_setpoint,temp_ramp, interval=10, md = None):

    """
    This plan will set gas mfc and temperature controllers and wait for 
    the temperature to reach a required value "temp_setpoint"

    During the temperature ramp the eiger will be triggered at an interval of "interval

    Once the temperature has been set, it will be held for "dwell" seconds
    During this time the eiger will be triggered as before 
    
    Parameters
    ------------
    gas_sel: string
        The gas to be selected ('He', 'H2', 'CO')
    temp_setpoint : float
        temperature to be reached. Until this temperature is reached, stay in this loop
    temp_ramp : float 
        degrees/ min to ramp at. Can be positive or negative. If negative there is just no heat
    interval : int
        seconds between triggers after an acquisition has finished
    md : dict
        A dictionary of additional metadata


    """

    # Set the temperature ramp rate
    yield from bps.abs_set(reactor_cell.temperature_reg.ramp, float(temp_ramp), wait=True)    

    # Set the Gas
    print(f"Setting the gas to {gas_sel} -> {gas_flows[gas_sel]}")
    yield from set_gas(gas_flows[str(gas_sel)])

    # yield from set_abs(reactor_cell.temperature_reg, temp_setpoint)
    print(f"Starting to change the temperature to {temp_setpoint}")
    complete_status = yield Msg('set', reactor_cell.temperature_reg, temp_setpoint)

    # Since we have no readback from gas analysis on actual gas env, 
    # the only thing we can do is wait for a known time
    print(f"Starting the acquire loop while temperature is changing")
    while not complete_status.done:
        
        yield from bps.one_shot([reactor_cell.temperature_reg,reactor_cell.temperature_sam, kth00,kth01]) 

        yield from bps.checkpoint()
        yield from sleep(interval)
    
    # We are actually never waiting on the gas to get anywhere
    #gas_status = Status(timeout= min_gas_time)

    # Read but don't save the temperature value
    val = yield from rd(reactor_cell.temperature_reg.value)
    print(f"Temperature Achieved: {val}C" )

    
def dwell_and_xrd(gas_sel,dwell, interval=10, md = None):

    """
    This plan will set gas mfc and wait there for a given amount of time

    During this period the eiger will be triggered 
    A delay of "interval" can be set between triggers
    
    Parameters
    ------------
    gas_sel: string
        The gas to be selected ('He', 'H2', 'CO')
    dwell : int 
        seconds to dwell at this state for
    interval : int
        seconds between triggers after an acquisition has finished
    md : dict
        A dictionary of additional metadata

    -------

    """

    # Set the Gas
    print(f"Setting the gas to {gas_sel} -> {gas_flows[gas_sel]}")
    yield from set_gas(gas_flows[str(gas_sel)])

    # Now stay here for a given amount of time
    dwell_status = Status(timeout=dwell)
    while not dwell_status.done:
        yield from bps.one_shot([reactor_cell.temperature_reg,reactor_cell.temperature_sam,kth00,kth01]) #triggers and reads everything in the detectors list
        yield from bps.checkpoint()
        yield from sleep(interval)
                

import numpy as np


def cat1():

    # interval 0, num_exp = 1

    # flushing 20 min with gas
    #scare_factor = 1.5
    #time_per_frame = eiger.cam.acquire_time.get()+2 # aDD 9 seconds because we observe ea
    #total_experiment_time=480 #about 8 hours, calucluate it later
    #num_frames = np.ceil(time_per_frame*total_experiment_time*scare_factor)
    
    #print(f"the estimated number of total images is {num_frames}")


    #yield from bps.abs_set(eiger.cam.num_triggers, num_frames, wait=True)

    md = {'sample':'sample 2' , 'project': 'care-o-scene', 'operator': 'Catalina'}

    @bpp.stage_decorator([kth00,kth01])
    @bpp.run_decorator(md = md)
    def inner_plan():
        #Times all ramps by 10 and dwell divided by 60
        yield from ramp_and_xrd(gas_sel='H2', temp_setpoint=35, temp_ramp=5)
        yield from dwell_and_xrd(gas_sel='H2',dwell=5*60)

        yield from ramp_and_xrd(gas_sel='H2', temp_setpoint=426, temp_ramp=3)
        yield from dwell_and_xrd(gas_sel='H2',dwell=150*60)

        yield from dwell_and_xrd(gas_sel='He',dwell=10*60)
        yield from ramp_and_xrd(gas_sel='He', temp_setpoint=223, temp_ramp=-20)
        yield from dwell_and_xrd(gas_sel='He',dwell=10*60)

        yield from dwell_and_xrd(gas_sel='CO',dwell=2*60)
        yield from dwell_and_xrd(gas_sel='CO', temp_setpoint=264, temp_ramp=3)
        yield from dwell_and_xrd(gas_sel='CO',dwell=300*60)
        yield from dwell_and_xrd(gas_sel='CO', temp_setpoint=219, temp_ramp=-20)
        yield from dwell_and_xrd(gas_sel='CO',dwell=20*60)
        
        yield from dwell_and_xrd(gas_sel='He',dwell=20*60)

        yield from dwell_and_xrd(gas_sel='H2',dwell=20*60)
        yield from dwell_and_xrd(gas_sel='H2', temp_setpoint=481, temp_ramp=3)
        yield from dwell_and_xrd(gas_sel='H2',dwell=150*60)
        yield from dwell_and_xrd(gas_sel='H2', temp_setpoint=211, temp_ramp=20)
        yield from dwell_and_xrd(gas_sel='H2',dwell=120*60)
        #add cooling stuff


    return (yield from inner_plan())

    


