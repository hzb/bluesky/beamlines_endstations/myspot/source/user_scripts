# start of automatically prepended lines
from beamlinetools.BEAMLINE_CONFIG import *
# end of automatically prepended lines
from bluesky.plans import scan
from bluesky.plan_stubs import sleep
from bluesky.utils import (
    separate_devices,
    all_safe_rewind,
    Msg,
    ensure_generator,
    short_uid as _short_uid,
)
import time
from ophyd.status import Status


# Define the gas mix for a given gas selection in ml/min
gas_flows = {"He":(10, 0,0),"H2":(0,12,0), "CO":(0,0,3)}

def set_gas(mfc_tuple):
    yield from mv(gas_dosing.massflow_contr1, mfc_tuple[0],gas_dosing.massflow_contr2, mfc_tuple[1],gas_dosing.massflow_contr3, mfc_tuple[2])
    
def ramp_and_xrd(gas_sel, temp_setpoint,temp_ramp, dwell, interval=5,min_gas_time=0, num_exp=6):

    """
    This plan will set gas mfc and temperature controllers and wait for

    1. A given amount of time to elapse to account for the gas to change
    2. The temperature to reach a required value.

    During the temperature ramp the eiger will be triggered "num_exp" times 
    A delay of "interval" can be set between groups of eiger triggers

    Once the temperature has been set, it will be held for "dwell" seconds
    During this time the eiger will be triggered as before 
    
    Parameters
    ------------
    gas_sel: 


    -------


    
    assumed gas flows
    h = 12 ml/min
    hel = 10 ml /min
    c0 =3ml/min

    Assumed that mfc1 is H2, mfc2 is He, MFC3 is CO
    """

    
    
    # Set the Gas
    print(f"Setting the gas to {gas_sel} -> {gas_flows[gas_sel]}")
    yield from set_gas(gas_flows[str(gas_sel)])

    # yield from set_abs(reactor_cell.temperature_reg, temp_setpoint)
    print(f"Starting to change the temperature to {temp_setpoint}")
    complete_status = yield Msg('set', reactor_cell.temperature_reg, temp_setpoint)
    # Since we have no readback from gas analysis on actual gas env, 
    # the only thing we can do is wait for a known time
    print(f"Starting the acquire loop while temperature is changing")
    while not complete_status.done:
        
        yield from count([eiger],num_exp)
        print(f"sleeping {interval} sec")
        yield from sleep(interval)
        print(f"done sleeping" )
    
    gas_status = Status(timeout= min_gas_time)

    val = yield from rd(reactor_cell.temperature_reg.value)
    

    print(f"Temperature Achieved: {val}C" )

    # Now stay here for a given amount of time
    dwell_status = Status(timeout=dwell)
    while not dwell_status.done:
        print("Waiting")
        yield from count([eiger],num_exp)
        yield from sleep(interval)
                
    print(f"Dwell time elapsed" )

    

def cat1():

    # interval 0, num_exp = 1

    # flushing 20 min with gas
    
    yield from ramp_and_xrd(gas_sel='H2', temp_setpoint=35, temp_ramp=5,dwell=1*60)

    yield from ramp_and_xrd(gas_sel='H2', temp_setpoint=426, temp_ramp=5,dwell=40*60)

    yield from ramp_and_xrd(gas_sel='He', temp_setpoint=426, temp_ramp=5,dwell=10*60)
    yield from ramp_and_xrd(gas_sel='He', temp_setpoint=223, temp_ramp=20,dwell=10*60)

    yield from ramp_and_xrd(gas_sel='CO', temp_setpoint=223, temp_ramp=10,dwell=10*60)
    yield from ramp_and_xrd(gas_sel='CO', temp_setpoint=264, temp_ramp=5,dwell=300*60)
    yield from ramp_and_xrd(gas_sel='CO', temp_setpoint=219, temp_ramp=20,dwell=20*60)
    
    yield from ramp_and_xrd(gas_sel='He', temp_setpoint=211, temp_ramp=5,dwell=20*60)

    yield from ramp_and_xrd(gas_sel='H2', temp_setpoint=211, temp_ramp=5,dwell=20*60)

    yield from ramp_and_xrd(gas_sel='H2', temp_setpoint=481, temp_ramp=5,dwell=150*60)
    yield from ramp_and_xrd(gas_sel='H2', temp_setpoint=211, temp_ramp=20,dwell=120*60)

    yield from ramp_and_xrd(gas_sel='He', temp_setpoint=35, temp_ramp=5,dwell=0)
    



    


